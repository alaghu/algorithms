Algorithms
==========

[![Build Status](https://travis-ci.org/alaghu/Algorithms.svg?branch=master)](https://travis-ci.org/alaghu/Algorithms)

[![Coverage Status](https://coveralls.io/repos/alaghu/Algorithms/badge.png?branch=master)](https://coveralls.io/r/alaghu/Algorithms?branch=master)

These are my attempt to learn Algorithms and Ruby. The  and exercises are based on Algorithms, Fourth Edition
by Robert Sedgewink and Kevin Wayne. Though the book uses Java, I have attempted a selection of exercises in ruby.On a similair note, I have tried to stick to Test Driven Development by using rspec.

Thanks
